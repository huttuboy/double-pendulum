#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Double Pendulum
# ===============
# Tuomo Huttu
# 13.9.2019 (video save added 12.7.2020)
# ===============


# sudo apt-get install python-dev build-essential libssl-dev libffi-devlibxml2-dev libxslt1-dev zlib1g-dev python-pip python-tk
# pip install numpy scipy matplotlib

# https://en.wikipedia.org/wiki/Double_pendulum
# https://www.wolframalpha.com/input/?i=double+pendulum
# http://sophia.dtp.fmph.uniba.sk/~kovacik/doublePendulum.pdf


import sys
from matplotlib.pyplot import figure,axes,subplots,show
import matplotlib.animation as anim
from numpy import sin,cos,linspace
from scipy.integrate import odeint


#angles(rad) at start
theta1=2.1
theta2=2.5

#gravity(m/s^2)
G=9.81

#masses(kg),lengths(m)
m1,m2=1.,1.
L1,L2=1.,1.

#sim-time(s)
time=30

line,line2=None,None
x1,x2,y1,y2=None,None,None,None

def deriv(y,t):
    global m1,m2,L1,L2,G
    
    #theta1,theta2
    th1=y[0]
    th2=y[2]

    # "The dot-notation indicates the time derivative of the variable in question."
    # "velocity is the derivative with respect to time of the position"
    # "acceleration is the second derivative"
    
    #theta1,theta2 dots
    dth1=y[1]
    dth2=y[3]

    #theta1,theta2 dotdots
    #(equations: http://sophia.dtp.fmph.uniba.sk/~kovacik/doublePendulum.pdf)
    apu=th1-th2
    ddth1=((-m2*L1*dth1*dth1*sin(apu)*cos(apu)
        +G*m2*sin(th2)*cos(apu)
        -m2*L2*dth2*dth2*sin(apu)
        -(m1+m2)*G*sin(th1)) 
        / 
        (L1*(m1+m2)
        -m2*L1*cos(apu)*cos(apu)))
    ddth2=((m2*L2*dth2*dth2*sin(apu)*cos(apu)
        +G*sin(th1)*cos(apu)*(m1+m2)
        +L1*dth1*dth1*sin(apu)*(m1+m2)
        -G*sin(th2)*(m1+m2)) 
        / 
        (L2*(m1+m2)
        -m2*L2*cos(apu)*cos(apu)))
    
    return dth1,ddth1,dth2,ddth2



def main():
    global theta1,theta2,time,line,line2
    global x1,x2,y1,y2
    global L1,L2

    #init.state
    y0=[theta1,0.,theta2,0.]

    timepoints=linspace(0,time,time*50)
    
    y=odeint(deriv,y0,timepoints)
    #print y
    x1=L1*sin(y[:,0])
    y1=-L1*cos(y[:,0])
    x2=x1+L2*sin(y[:,2])
    y2=y1-L2*cos(y[:,2])

    #fig,ax=subplots()
    fig=figure()
    ax=axes(xlim=(-(L1+L2+.2),L1+L2+.2),ylim=(-(L1+L2+.2),L1+L2+.2))
    ax.set_aspect('equal')
    
    line,=ax.plot([],[],'o-',lw=3)
    line2,=ax.plot([],[],'r-',lw=1)
    
    #https://matplotlib.org/2.0.2/examples/animation/simple_anim.html
    anim0=anim.FuncAnimation(fig,animate,range(2,len(y)),interval=1)

    #Writer = anim.writers['ffmpeg']
    #writer = Writer(fps=25, bitrate=3600)
    #anim0.save("demo.mp4",writer=writer)
    show()


def animate(i):
    global line,line2
    line.set_data([0,x1[i],x2[i]],[0,y1[i],y2[i]])
    line2.set_data(x2[:i],y2[:i])
    return line,line2



if __name__ == "__main__":
    main()

