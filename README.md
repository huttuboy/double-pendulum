# Double pendulum

Ohjelma toteuttaa tuplaheilurin Python2.7-kielellä.

Muuttujina koodissa ovat:
* putoamiskiihtyvyys
* tankojen lähtökulmat ja pituudet
* kuulien massat
* simulaatio-aika

Tekijä Tuomo Huttu
